<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>

   <?php print $head; ?>
   <title><?php print $head_title; ?></title>
   <?php print $styles; ?>
   <?php print $scripts; ?>

   <!--[if lte IE 7]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . $directory; ?>/ie7.css" >
   <![endif]--> 

   <!--[if lt IE 7]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . $directory; ?>/ie.css" >
   <![endif]--> 

</head>

<body>

   <div id = "header_upper" class = "clear_both">
      <?php if ($header_upper): print $header_upper; endif; ?>
   </div>   

   <div id = "header_block"><?php print $header_block; ?></div>

   <div class = "header">  

      <div class = "header_body">

	<div id = "header_menu">
           <?php if (isset($primary_links)) : ?>
              <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
           <?php endif; ?>
	</div>

	<div class = "site_attributes">

	   <?php if ($logo): ?>
              <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>">
                 <img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" id="logo"></img>
	      </a>	
           <?php endif; ?>

	   <?php if ($site_name): ?>
              <h1>
		 <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>">
	            <div id = "site_name">
		       <?php print $site_name; ?>
		    </div>
	         </a>
	      </h1>
           <?php endif; ?>
	
	</div>

	<div class = "header_img"> </div>

      </div>	

   </div>

   <div id = "container_1">	
      <?php if (isset($secondary_links) && ($secondary_links)): ?>
	 <div id = "content_menu">
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
	 </div>
      <?php endif; ?>
 
	<?php if ($sidebar_left) { ?>
	   <div id = "sidebar_left">
		<?php print $sidebar_left; ?>
	   </div>
	<?php } ?>

	<div id = "content_body">

           <?php if ($content_top) { ?>
	      <div id = "content_top">
	         <?php print $content_top; ?>
	      </div>
	   <?php } ?>	

          <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <?php print $content; ?>  

	</div>

     </div>

	<?php if ($sidebar_right) { ?>
	   <div id = "sidebar_right">
		<?php print $sidebar_right; ?>
	   </div>
	<?php } ?>

   <div id = "footer" class = "clear_both">

	<div id = "footer_menu">
	   <?php if (isset($primary_links)): ?>
              <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
           <?php endif; ?>
	</div>

	<div id = "footer_counters"><?php print $footer_counters; ?></div>
	<div id = "footer_content"><?php print $footer_content; ?><?php print $feed_icons; ?> </div>
	<div id = "footer_copyright"><?php print $footer_copyright; ?>
	   <br><a href = "http://www.internet-marketing.by" title = "Web studia" >Web studia</a> for <a href = "http://bug.by" title = "Brest" >Brest</a>
	</div>
	 
   </div>

  <?php print $closure; ?>

  </body>
</html>
