<?php

function imbug_preprocess_node(&$vars) {
  $vars['node_bottom'] = theme('blocks', 'node_bottom');
}

function imbug_comment_form($form) {

  if ($form['_author']) {
    unset($form['_author']['#type']);
  }

  unset($form['comment_filter']['format']);

  $form['comment_filter'][1] = array(
    '#type' => 'value',
    '#value' => variable_get('filter_default_format', 1),
  );
  $tips = _filter_tips(variable_get('filter_default_format', 1), FALSE);
  $form['comment_filter']['format']['guidelines'] = array(
    '#title' => t('Formatting guidelines'),
    '#value' => theme('filter_tips', $tips, FALSE, $extra),
  );

  unset($form['homepage']);

  $form['comment_filter']['comment']['#title'] = '';
  $form['comment_filter']['comment']['#rows'] = '7';

  if (($form['mail'])&&($form['name'])) {
    $form['name']['#prefix'] = '<div class="side">';
    $form['name']['#suffix'] = '</div>';
    $form['mail']['#prefix'] = '<div class="side">';
    $form['mail']['#suffix'] = '</div>';

    $form['comment_filter']['#prefix'] = '<div class="main">';
    $form['comment_filter']['#suffix'] = '</div>';
  }

  $form['comment_filter']['format']['guidelines']['#prefix'] = '<div class="guidelines nolink">';
  $form['comment_filter']['format']['guidelines']['#suffix'] = '</div>';

  $output .= drupal_render($form);
  return $output;
}
