<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="<?php print 'block block-' . $block->module . ' block-' . $block_id . ' block-' . $block->extraclass . ' block-' . $block_id . '-' . $block->extraclass; ?>">
	
<?php if (!empty($block->subject)): ?>
   <div class = "block_title"><div class = "block_title_tr">	   
      <h2><?php print $block->subject; ?></h2>	   
   </div></div>
<?php endif; ?>

<div class = "block_content"><div class = "block_content_bl">
   <?php print $block->content; ?>
</div></div>

</div>
