<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

   <div class = "node_title"><div class = "node_title_tr">
      <?php if ($title): ?>
         <h2><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
   </div></div>

   <div class = "node_submitted">
      <?php if ($submitted): ?>
         <span class="submitted"><?php print $submitted; ?></span>
      <?php endif; ?>
   </div>

   <div class = "node_body"><div class = "node_body_bl">
      <div class="content clear-block">
         <?php print $content; ?>
      </div>

      <div class="clear-block">
	 <?php if ($taxonomy): ?>
            <div class="meta">            
               <div class="terms"><?php print $terms; ?></div>            
            </div>
	 <?php endif; ?>
      </div>

   </div></div>

   <?php if ($links): ?>
      <div class="actions"><?php print $links; ?></div>
   <?php endif; ?>

   <?php if ($node_bottom && !$teaser) { ?>
      <div id = "node_bottom">
	  <?php print $node_bottom; ?>
      </div>
   <?php } ?>

</div>