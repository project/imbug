﻿<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">  

   <div class = "comment_title"><div class = "comment_title_tr">
      <h3><?php print $title; ?></h3>
   </div></div>

   <div class="submitted"><?php print $submitted; ?></div>

   <div class = "comment_body">

     <div class="content">
        <?php print $content; ?>
        <?php if ($signature): ?>
           <div>—</div>
	   <?php print $signature; ?>
        <?php endif; ?>
     </div>

     <?php print $picture; ?>

   </div>

   <?php if ($links): ?>   
      <div class = "comment_footer"><div class = "comment_footer_bl"> 
         <div class="links">
            <?php print $links; ?>
         </div>
         <img src="<?php print base_path() . path_to_theme(); ?>/images/blog_pencil.gif" id="img_quote" title="Выделите текст и нажмите эту кнопку, чтобы вставить цитату" onmouseover="getText('<?php print $comment->name; ?>');" onclick="insertQuote();">
      </div></div>
   <?php endif; ?>

</div>
